from SOAI.SOAIDataHandler import SOAIDataHandler
import logging

logger = logging.getLogger()

dataHandler = SOAIDataHandler()

dataOpenAir = dataHandler.fGetOpenAir("./data/openair/")
dataLanuv = dataHandler.fGetLanuv("./data/lanuv/")

logger.debug("Lanuv and OpenAirData is available")
