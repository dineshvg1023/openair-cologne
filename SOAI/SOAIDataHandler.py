import pandas as pd
import glob
import logging

logger = logging.getLogger()

## Class which handles the loading of data
class SOAIDataHandler:

    def __init__(self):
        pass

    ## Load data from OpenAir Cologne
    def fGetOpenAir(self, path="./data/openair/"):
        logger.debug(f"Load OpenAir Cologne data from {path}.")

        filesOpenAir = glob.glob(path+"*.parquet")
        listOpenAir = []
        for filename in filesOpenAir:
            logger.debug(f"\t- Load {filename}")
            df = pd.read_parquet(filename)
            #df = df.set_index(pd.DatetimeIndex(df["timestamp"]))
            #df.drop(["timestamp"], axis=1)

            listOpenAir.append(df)

        dataOpenAir = pd.concat(listOpenAir)
        dataOpenAir = dataOpenAir.sort_values("timestamp").reset_index(drop=True)

        return dataOpenAir

    ## Load data from Luftdaten.info
    def fGetLanuv(self, path= "./data/lanuv/"):
        logger.debug(f"Load Lanuv data from {path}.")

        filesLanuv = glob.glob(path+"*.parquet")
        listLanuv = []
        for filename in filesLanuv:
            logger.debug(f"\t- Load {filename}")
            df = pd.read_parquet(filename)
            listLanuv.append(df)

        dataLanuv = pd.concat(listLanuv, ignore_index=True)
        dataLanuv = dataLanuv.sort_values("timestamp").reset_index(drop=True)

        return dataLanuv

