# OpenAir Cologne

### School of AI Cologne project in cooperation with OpenAir Cologne.

-------------------

In order to run the project perform the following steps:
- Make sure you have *git* installed
- Clone the repository with **git clone https://gitlab.com/N.Stausberg/openair-cologne**
- Change to a unique branch with **git checkout -b someName**
- Inside the downloaded repository create logging folder **mkdir logs**
- Install all packages in *requirements.txt* e.g in some virtual enviroment
- Run **python example.py**
